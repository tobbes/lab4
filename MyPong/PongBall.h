//
//  PongBall.h
//  MyPong
//
//  Created by Tobias Ednersson on 2015-02-09.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
// Class to represent the padel


#import <Foundation/Foundation.h>

@interface PongBall : NSObject

@property (nonatomic) struct Position  pos;
@property (nonatomic) float VerticalSpeed;
@property (nonatomic) float HorizontalSpeed;
@property (nonatomic) struct Dimensions  size;
@property  (nonatomic) float currentSpeedFactor;
@property (nonatomic) float Speed;


- (void) setBallPositionX:(float) x Y: (float) y;
-(void) flipSpeed:(BOOL) horizontal;

-(void) updatePosition;


//representations of size and position on screen
struct Dimensions {
    float x;
    float y;
};


struct Position {
    float x;
    float y;
};




@end
