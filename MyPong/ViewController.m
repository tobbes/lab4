//
//  ViewController.m
//  MyPong
//
//  Created by Tobias Ednersson on 2015-02-09.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "ViewController.h"
#import "PongBackend.h"
# import "PongPadel.h"//;
#import <limits.h>
@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIView *PongRacket;
@property (weak, nonatomic) IBOutlet UIView *PongBall;
@property (nonatomic) PongBackend * backend;
@property (nonatomic) NSTimer * Gametimer;
@property (nonatomic,strong) PongPadel * padle;
@property (weak, nonatomic) IBOutlet UILabel *ScoreLabel;


@end

@implementation ViewController


-(PongPadel *) padle {
    return _padle;
    
}



- (IBAction)FingerPanned:(UIPanGestureRecognizer *)sender {
 
    
    if(sender.state == UIGestureRecognizerStateBegan) {
        
        CGPoint point = [sender locationInView:self.view];
    
        if(CGRectContainsPoint(self.PongRacket.frame, point)) {
            self.backend.padel.isMoving = YES;
            
            
        }
    
    }
    
        
    
    
    else if(sender.state == UIGestureRecognizerStateEnded)
    {
        
        self.backend.padel.isMoving = NO;
    }
    
    
    if(self.backend.padel.isMoving) {
    
    CGPoint p =  [sender locationInView:self.view];

    CGRect newPos = CGRectMake(p.x, self.PongRacket.frame.origin.y, self.PongRacket.frame.size.width, self.PongRacket.frame.size.height);
    
        
        self.PongRacket.frame = newPos;
        
        [self.padle setPositionX:self.PongRacket.center.x Y:self.PongRacket.center.y];
        
    
    }

}

-(PongBackend *) backend {
    
    if(!_backend) {
        
        NSLog(@"Vidd:  %f Hojd: %f", self.PongBall.superview.frame.size.width, self.PongBall.superview.frame.size.height);
        _backend = [[PongBackend alloc]initWithScreenWidth:self.PongBall.superview.frame.size.width andHeight:self.PongBall.superview.frame.size.height];
        }

    return _backend;

}


    

- (void) startTimer {
   
    if(self.Gametimer == nil) {
        self.Gametimer = [[NSTimer alloc]init];
    }
    
    self.Gametimer = [NSTimer scheduledTimerWithTimeInterval:0.25f target:self selector:@selector(MainGameLoop) userInfo:nil repeats:YES];

    
    
}



-(void) stopTimer {
    
    
    [self.Gametimer invalidate];
    self.Gametimer = nil;
}




/* Called 4 times per second by the nstimer instance, the main loop which drives the game */
-(void) MainGameLoop {
    
    //update the scorelabel with latest score
    // this, rather unexpectedly causes didlayoutsubviews to be called, which
    // menas that for every point earned by the player the ball gets a new starting
    // position...
    self.ScoreLabel.text = [NSString stringWithFormat:@"%d",self.backend.Score];

    
    
    // get the current frame (current position) of the pongball
    CGRect rect = self.PongBall.frame;
    
    
    //Right, so the backend pongball is set at the centre of
    // of the pongball view
    // so we subtract half the ball's width and half the balls height
    // to get upper left corner, since I specify upper left corner position of the
    // new rectangle
    float newX =  self.backend.ball.pos.x - self.PongBall.frame.size.width/2;
    float newY = self.backend.ball.pos.y - self.PongBall.frame.size.height/2;
    
    CGRect newRect = CGRectMake(newX, newY, rect.size.width, rect.size.height);

    
    self.PongBall.frame = newRect;
    [self.backend checkCollissions];
    [self.backend.ball updatePosition];
    
    
    //if the player lost the game
    // stop the timer then pause, reset score and
    // then restart the game.
    
    
    if(self.backend.Lost) {
        
        [self stopTimer];
        
        
        
        sleep(2);
        
        self.backend.Score = 0;
        [self startGame];
        
    }
    
    
  
    
    
    
    
    
    
    
    
    
    
    
}





- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidLayoutSubviews {
    
    [self startGame];
    
    
}



/* Restarts the game reandomizes a new starting position for the ball
 */
-(void) startGame {
    
    
    float startX = arc4random() % (int)(self.view.frame.size.width - self.PongBall.frame.size.width);
    float startY = arc4random() % (int) self.view.frame.size.height/2;
    
    // Get random direction
    
   
    

    

    
    
    
    
    CGRect rect = CGRectMake(startX, startY ,
                             self.PongBall.frame.size.width, self.PongBall.frame.size.height);
    
    
    
    
    UIView * rem = self.PongBall;
    
    [rem removeFromSuperview];
    
    
    
    
    UIView * v = [[UIView alloc]initWithFrame:rect];
    
    v.backgroundColor = [UIColor redColor];
    
    self.PongBall = v;
    
    [self.view addSubview:v];
    
    
    //self.PongBall = v;
    
    
    
    
    
    
    //self.PongBall.frame = rect;
    //float xpos = self.PongBall.frame.origin.x;
    float xpos = self.PongBall.center.x;
    float ypos = self.PongBall.center.y;
    
    
   
    long outcome = arc4random()/LONG_MAX;
    
    //left
    if(outcome < 0.3l)
    {
        [self.backend.ball setHorizontalSpeed:-10.0f * (1 + self.backend.Score/10)];
        
        
    }
    
    //straight down or straight up
    else if (outcome < 0.6l) {
        [self.backend.ball setHorizontalSpeed:0.0f];
        
    }
    
    
    //right
    
    else {
        
        [self.backend.ball setHorizontalSpeed:10.0f * (1 + self.backend.Score/10)];
    }
    
    
    outcome = arc4random();
    
    
    //ball will always be travelling upwards or downwards
    
    if(outcome < 0.5f ) {
        
        [self.backend.ball setVerticalSpeed:-10.0f * (1 + self.backend.Score/10)];
    }
    
    else {
        [self.backend.ball setVerticalSpeed:10.0f * (1 + self.backend.Score/10)];
        
    }
    
    
    
    
    
    
    [self.backend.ball setBallPositionX:xpos Y:ypos];
    //[self.backend.ball setVerticalSpeed:10.0f];
   
    
    struct Dimensions d;
    d.x = self.PongBall.frame.size.width;
    d.y = self.PongBall.frame.size.height;
    
    
    
    [self.backend.ball setSize:d];
    
    self.backend.padel = [[PongPadel alloc]init];
    self.padle = self.backend.padel;
    ;
    
    
    
    
    
    [self.backend.padel setSize: self.PongRacket.frame.size.width And: self.PongRacket.frame.size.height];
    
    [self.backend.padel setPositionX:self.PongRacket.center.x Y:self.PongRacket.center.y];
    
    
    
    
    
    
    
    if(self.Gametimer) {
        [self stopTimer];
        
    }
    
    self.backend.Lost = NO;
    
    [self startTimer];
    
    
    

    
}

@end
