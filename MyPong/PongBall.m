//
//  PongBall.m
//  MyPong
//
//  Created by Tobias Ednersson on 2015-02-09.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//



#import "PongBall.h"




@interface PongBall ()



@end;

@implementation PongBall
@synthesize  pos = _pos;
@synthesize VerticalSpeed = _VerticalSpeed;
@synthesize HorizontalSpeed = _HorizontalSpeed;
@synthesize size = _Size;

- (struct Position)pos {
    
    return _pos;
}

@synthesize currentSpeedFactor = _currentSpeedFactor;


-(void)setCurrentSpeedFactor:(float)currentSpeedFactor {
    _currentSpeedFactor = currentSpeedFactor;
}


-(float) currentSpeedFactor {
    
    return _currentSpeedFactor;
}






-(void) setPos:(struct Position)pos {
    
    _pos = pos;
}


-(float) VerticalSpeed {
    
    
    return _VerticalSpeed;
}


-(void)setVerticalSpeed:(float)VerticalSpeed {
    
    _VerticalSpeed = VerticalSpeed;
}

-(struct Dimensions) size {
    
    return _Size;
}


-(void) setSize:(struct Dimensions)size {
    _Size = size;
}


-(instancetype) initWithPos:(struct Position) pos andSize: (struct Dimensions) dims {
    
    self = [super init];
   
    if(self) {
        
        self.size = dims;
        self.pos = pos;
    }
    
    return self;
}

/* 
 An easier way to set the ball's position by specifying x and y coordinates
 */
- (void) setBallPositionX:(float) x Y: (float) y {

    struct Position newPos = {x,y};
    
    self.pos = newPos;
}


// called each iteration of the game to update the position of the ball
-(void) updatePosition {
    
    [self setBallPositionX:self.pos.x + self.HorizontalSpeed Y:self.pos.y + self.VerticalSpeed];
    
   
    
}


// inverts the direction of the ball
//  YES to invert the horizontal direction
// NO  to invert the vertical direction
- (void) flipSpeed:(BOOL) horizontal {
    
    if(horizontal) {
        
        self.HorizontalSpeed = - self.HorizontalSpeed;
        
    }
    else {
        self.VerticalSpeed = - self.VerticalSpeed;
        
    }
    
}







    
    
    
    



    


















@end
