//
//  PongBackend.h
//  MyPong
//  Created by Tobias Ednersson on 2015-02-09.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
// This class contains (monst of) the gamelogic 

#import <Foundation/Foundation.h>
#import "PongBall.h"
#import "PongPadel.h"

@interface PongBackend : NSObject
@property (nonatomic, strong) PongBall * ball;
@property (nonatomic, strong) PongPadel * padel;
@property (nonatomic) int Score;
@property (nonatomic) BOOL Lost;

-(instancetype) initWithScreenWidth:(float) width andHeight: (float) height;


-(void) checkCollissions;

@end

    
    
