//
//  PongPadel.h
//  MyPong
//
//  Created by Tobias Ednersson on 2015-02-09.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
// Class representing the padle of the game

#import <Foundation/Foundation.h>


@interface PongPadel : NSObject

@property (nonatomic) BOOL isMoving;
@property (nonatomic)  struct Position pos;
@property (nonatomic) struct Dimensions size;
- (void) setSize:(float) width And: (float) height;
-(instancetype) init;
- (void) setPositionX: (float) x Y: (float) y;

@end
