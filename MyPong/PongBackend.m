//
//  PongBackend.m
//  MyPong
//
//  Created by Tobias Ednersson on 2015-02-09.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "PongBackend.h"
#import "PongBall.h"//;

@interface PongBackend ()

//these are set at the start of the game by the
// viewcontroller
@property (nonatomic) float ScreenWidth;
@property (nonatomic) float ScreenHeight;

@end
@implementation PongBackend

@synthesize ball = _ball;
@synthesize ScreenWidth = _ScreenWidth;
@synthesize ScreenHeight = _ScreenHeight;
@synthesize padel = _padel;
@synthesize Score = _score;
@synthesize Lost = _Lost;






-(void) setLost:(BOOL)Lost {
    _Lost =  Lost;
}


-(BOOL) Lost {
    return  _Lost;
}




//never got around to use this...
/*-(void) calculateRealSpeed {
    
    
    float pixelsperSec = self.ScreenHeight / self.ball.currentSpeedFactor;
    
    self.ball.Speed =  pixelsperSec * 0.25;
    
    
}*/


//gamepadel instance
-(PongPadel *) padel {
    
    if(!_padel) {
        _padel = [[PongPadel alloc] init];
    }
    return _padel;
}

-(void) setPadel:(PongPadel *)padel {
    
    _padel = padel;
}


//the player's score
-(void) setScore:(int)Score {
    _score = Score;
}


-(int) Score {
    
    if(!_score) {
        _score = 0;
    }
    return _score;

}







- (float) ScreenWidth {
    
    return _ScreenWidth;
}

- (void) setScreenWidth:(float)ScreenWidth {
    _ScreenWidth = ScreenWidth;
}

-(float) ScreenHeight {
    
    return _ScreenHeight;
}


-(void) setScreenHeight:(float)ScreenHeight {
    _ScreenHeight = ScreenHeight;
}





//instance of ball
- (PongBall *) ball {
    
    if(!_ball) {
        _ball = [[PongBall alloc] init];
    }
    return _ball;
}

- (void) setBall:(PongBall *)ball {
    
    _ball = ball;
}


//this constructor called with screen witdth and screen height of the device
-(instancetype) initWithScreenWidth:(float) width andHeight: (float) height {
    self = [super init];
    
    
    if(self) {
        self.ScreenHeight = height;
        self.ScreenWidth = width;
     
        
    }
    
    return self;
}



/* checks if the ball has collided with either any of the edges of the game area
 or with the padel */


-(void) checkCollissions {
    
    
    //y-dimension
    
    //collides with screen-top
    
    if(self.ball.pos.y <=  (0 + self.ball.size.y/2)) {
    
        //one more point for the player
        self.Score++;
        
        //flip the vertical speed so that the ball moves downwards
        [self.ball flipSpeed:NO];
        
    }
    
    
    //collides with screen bottom
    else if(self.ball.pos.y >= self.ScreenHeight - self.ball.size.y/2)
    {
        
        
        struct Position p = {self.ball.pos.x,self.ScreenHeight - self.ball.size.y/2};
        
        self.ball.pos = p;
        
        
        
        self.Lost = YES;
        
        
        // make the ball go upwards
        [self.ball flipSpeed:NO];
        
    }

    
    
    
    
    
    
    
    
    //xdimension
    
    
    //collides with left edge
    if(self.ball.pos.x <= (0 + self.ball.size.x/2))
    {
        
        //make the ball go right
        [self.ball flipSpeed:YES];
    }
    
    
    
    
    //collides with right edge
    else if(self.ball.pos.x >= self.ScreenWidth - self.ball.size.x/2 ) {
        
        //make the ball go left
        [self.ball flipSpeed:YES];
    }
    
    
    
    
    
    //find the difference between the centerpoint of the ball and the centerpoint of the pad
    // and make sure the difference is allways positive
    float diffX = fabsf(self.ball.pos.x - self.padel.pos.x);
    
    float diffY = fabsf(self.ball.pos.y - self.padel.pos.y);
    
    
    // determine if collision takes playce
    if(diffY <= (self.padel.size.y/2 + self.ball.size.y/2) && diffX <= (self.ball.size.x/2 + self.padel.size.x/2) ) {
        
        
        
        
        
        
        
        
        
       
        //now determine which part of the paddle the ball hits
        
        //ball hits left third of padle
        // go right
        if( self.ball.pos.x <  (self.padel.pos.x - self.padel.size.x/3))
        {
            
        
            
       
            // the ball goes right also take into account the number of points the
            // player has gained more points => faster ball
            self.ball.HorizontalSpeed = 10.0f * (1+self.Score/10);
            
            
            
        }
        
        
        
        // ball hits center third of the paddle go straight up (ignore previous direction....)
        
        else if(self.ball.pos.x <= self.padel.pos.x + self.padel.size.x/3) {
            
            self.ball.HorizontalSpeed = 0.0f;
        }
        
        
        // ball hits right third of paddle go left...
        else {
            
            self.ball.HorizontalSpeed = -10.0f * (1+ self.Score/10);
        }
        
        
        
        //reverse vertical speed
        [self.ball flipSpeed:NO];
                
        
        
        
    }
    
    
    
    
    
    
    
    
}
    








@end




