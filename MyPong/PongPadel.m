//
//  PongPadel.m
//  MyPong
//
//  Created by Tobias Ednersson on 2015-02-09.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//




#import "PongBall.h"
#import "PongPadel.h"


@interface PongPadel ()



@end





@implementation PongPadel
@synthesize isMoving = _isMoving;
@synthesize pos = _pos;
@synthesize size = _size;



-(struct Dimensions) size {
    
    return _size;
}



-(void) setSize:(struct Dimensions)size {
    _size = size;
    
}






-(void) setSize:(float) width And: (float) height {
    
    struct Dimensions d;
    
    d.x = width;
    d.y = height;
    
    
    self.size = d;
    
}




- (struct Position)pos {
    return _pos;
    
}

- (void) setPos:(struct Position)pos
{
    _pos = pos;
}




- (void) setPositionX: (float) x Y: (float) y {
    
    struct Position p;
    

    p.y = y;
    p.x = x;
    self.pos = p;
        
}

-(BOOL) isMoving {
    
    return _isMoving;
}

-(void) setIsMoving:(BOOL)isMoving
{
    _isMoving = isMoving;
}

-(instancetype) init {
    
    self = [super init];
    
    
    if(self) {
        
        self.isMoving = NO;
    }
    
    
    
    return self;
 
    
    
    
    
    
}


@end
